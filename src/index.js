import React, { PropTypes } from 'react';
import { v4 } from 'uuid';
import { Col } from 'react-bootstrap';

import { MONTHS, range } from './utilities';

const styles = {
  heading: {
    color: 'red'
  }
}

class Calendar extends React.Component {
  constructor(props) {
    super(props);

    const date = new Date();
    
    this.state = {
      selectedMonth: date.getMonth(),
      selectedYear: date.getFullYear()
    };
  }

  changeSelectedYear = ({target: { value: selectedYear }}) => {
    this.setState({selectedYear});
  }

  changeSelectedMonth = ({target: { value: selectedMonth }}) => {
    const {lang} = this.props;

    this.setState({
      selectedMonth: MONTHS[lang].indexOf(selectedMonth)
    });
  }

  render() {
    const { lang, yearStart, yearEnd } = this.props;

    return (
      <Col xs={12}>
        <div style={styles.heading}>
          asdf asdf asdf asd
        </div>
        <Col md={12}>
          <Col xs={6}>
            <select value={MONTHS[lang][this.state.selectedMonth]} onChange={this.changeSelectedMonth}>
              {MONTHS[lang].map((month) => {
                return (
                  <option key={v4()}>{month}</option>
                );
              })}
            </select>
          </Col>
          <Col xs={6}>
            <select value={this.state.selectedYear} onChange={this.changeSelectedYear}>
              {[...range(yearStart, yearEnd + 1, 1)].map((year) => {
                return (
                  <option key={v4()}>{year}</option>
                );
              })}
            </select>
          </Col>
        </Col>
        <div>
          Calendar {MONTHS[lang][this.state.selectedMonth]} {this.state.selectedYear}
        </div>
      </Col>
    );
  }
}

Calendar.defaultProps = {
  lang: 'ru',
  yearStart: 2015,
  yearEnd: 2019
}

export default Calendar;
