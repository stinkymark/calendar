import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import Calendar from '../index';

storiesOf('Calendar', module)
  .add('default view', () => (
    <Calendar />
  ))
  .add('set russian lang', () => (
    <Calendar lang="ru" />
  ))
  .add('set english lang', () => (
    <Calendar lang="en" />
  ));
