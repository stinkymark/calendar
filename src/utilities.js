export const MONTHS = {
  ru: ['январь', 'февраль', 'март', 'апрель','май', 'июнь','июль', 'август','сентябрь', 'октябрь','ноябрь', 'декабрь'],
  en: ['january', 'february', 'march', 'april','may', 'june','july', 'august','september', 'october','november', 'december']
};

export function* range (start, end, step) {
    while (start < end) {
        yield start
        start += step
    }
}